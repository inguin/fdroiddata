Categories:Theming,Graphics
License:GPLv3,CC-BY-SA-4.0
Web Site:
Source Code:https://github.com/1C3/ICEcons
Issue Tracker:https://github.com/1C3/ICEcons/issues

Auto Name:ICEcons
Summary:OSS white icon pack
Description:
Icon pack for Trebuchet, Kiss, Nova, Apex, Holo and Adw launchers, also includes
4k wallpapers.
.

Repo Type:git
Repo:https://github.com/1C3/ICEcons.git

Build:2.1,5
    commit=2.1
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:2.1
Current Version Code:5
